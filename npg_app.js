/**
 * @file npg_app.js
 * @author frtk
 */


/**
 * nodejs modules
 */
//--- 
var fs = require('fs');
var express = require('express');
var http = require('http');


/**
 * npg server 
 */
//---
var cfg = require('./server/npg_server_config.js');
var npg = require('./server/npg_server.js');
npg.NPGServer.init(cfg.Config);


/**
 * HTTP
 */
//--- HTTP server
npg.NPGServer.log("$ # starting http service on port " + npg.NPGServer.port);
var app = express();
var httpserv = http.createServer(app);
httpserv.listen(npg.NPGServer.port);
//--- allow access to static files from "/client" directory
app.use(express.static(__dirname + '/client/'));


/**
 * socket.io
 */
//--- socket.io
npg.NPGServer.log('$ # registering socket.io service on port ' + npg.NPGServer.port);  
var io = require('socket.io').listen(httpserv, { log: true } );
//-- setup server socket handling features
npg.NPGServer.socketHandling(io);
npg.NPGServer.log('$ #####');



/**
 * run app
 */
//---

