

# **v0.2.2:**
## focus: Start Menu Page
--------------------------
### client objects update
- UICursor: added resetBlink() function to reset blink on menu item change (to avoid "shadow" cursor)
- UIMenu: added cursor resetBlink() in select next/previous item function
- UILabel: added UIObject inheritance and changed param initialization

### client objects creation
- UIStatusValue: ui element to display server infos (number of users, games running and games available) 

### client start menu page
- added 3 UIStatusValue elements
- all 3 elements are updated within the page_handler

### server 
- added server infos update for all clients when a user register or disconnect




# **v0.2.1:**
## focus: Start Menu Page
--------------------------
### client
- Added ui_menu.js (UIMenu object)
- Added cursor handling in UIMenu object
- Implemented cursor on "start menu" page 
fixed start menu (join/create/spectate)



# **v0.2.0:**
## focus: Start Menu Page
--------------------------
### client
- create start menu page skeletton with title
- socketio: on 'regDone' move current page to 'start menu' page




# **v0.1.5:**
##focus: client/server user registration 
-------------------------- 
### added socket messages in npg_server.js and client/lib/socket/socketio_handler.js 
'regRequest' server socket msg 
'regDone' client socket message
'regNameTaken' client socket message
### 
Added user handling utilities {
userRegistration
clientDisconnect
}
### added User object on server
check if user already exist and handle issues
### 
user disconnection handling (server & client)



### **v0.1.4:**
focus: client ui lib 
-------------------------- 
---- changed font (Lucida -> Arial)
- npg_client.js
---- added in /ui/objects/
- ui_object.js (base object)
---- updated objects in
- ui_rect.js  (inherits from UIObject)
- ui_cursor.js (inherits from UIObject)
---- /keyboard/evt_handler.js 
- userLogin()
---- cursor object
- fixed behaviour on login page - working fine now
- reset position at login attempt when server is down 


### **v0.1.3:**
-------------------------- 
focus: client ui lib - keyboard handling
-------------------------- 
---- /client/utils
- renamed utils.js -> client_utils.js
- added js_utils.js (global methods)
---- keyboard handling on login page
- input name feature (add and remove char)
---- added in /client/ui/objects
- ui_shape.js (base shape object)
- ui_cursor.js
---- removed in /client/ui/objects
- ui_line.js
- ui_object.js 
---- changed UIRect object
- inherits from UIShape
---- cursor object
- blinking cursor blinks (need to implement movement with name input)


### **v0.1.2:**
-------------------------- 
focus: client ui lib
-------------------------- 
---- added ui objects in /client/ui/objects
- ui_input_text.js
- ui_menu.js
- ui_status_text.js
- ui_text_style_.js
---- Login page
- changed convention for parameters in npg_client.js (by 'page' object)
- title label and server status working 
- skeletton for login name input


### **v0.1.1:**
-------------------------- 
focus: client ui lib
-------------------------- 
---- added app page handling in client/core
- page_base.js
- page_handler.js 
---- added ui handling in /client/ui
- ui.js
---- added ui objects in /client/ui/objects
- ui_label.js
- ui_line.js
- ui_object.js
- ui_point2d.js
- ui_rect.js
- ui_style.js
---- login page
- created login page with title


### **v0.1.0:**
-------------------------- 
focus: app
-------------------------- 
- updated npg_app.js



### **v0.0.9:**
-------------------------- focus: renaming files/directories
---- App
- nodePong.js -> npg_app.js
- package.json: updated "directories" & "dependencies" parameters
---- Client
- client/src -> client/jquery
- client/js -> client/lib
- updated index.html
---- Server
- server_core.js -> npg_server.js
- server_config.js -> npg_server_config.js


### **v0.0.8:**
--- focus on client side
- Renamed files (lower case + _ )
- Client directory structure : config, core, keyboard, pages, socket 
- added keyboard event handler basic features
- added AppPage object definition
- added login page skeletton
- nothing crashed


### **v0.0.7:**
--- focus on server object
- Renamed server object in server_core.js (Server -> NPGServer)
- Removed setIO(io) function in NPGServer object
- added init() function to NPGClient 
- Changed server_config.js structure
- updated nodePong.js 


### **v0.0.6:**
--- focus on socket.io client & server)
- Renamed SocketClient.js to SocketIO.js (files and objects)
- Added SocketIO object (NGPClient lib in /client/js/)
- Implemented socket.io client connection (with loop) => working
- Client side: Corrected bugs of multiple connection attemps and logs
- Server side: moved socket definitions to Server object


### **v0.0.5:**
- objects naming convention change in server_core.js, and consequently in nodePong.js
- added "User" and "Game" Objects skeletton in server_core.js
- added "version" as a parameter of core.Server object (to later send it to clients @ connection)


### **v0.0.4:**
- added server_core.js, with "core" app server side object skeletton
- added "serverMsg" features to "core" (including date utilities)
- removed server_utils.js (moved serverMsg() function to the core object)
- updated README.md (removed "npm install colors) 
- moved server.params.js to server_config.js, and changed naming convention
- updated nodePong.js with new config setup


### **v0.0.3:**
- Added infos in README.md


### **v0.0.2:**
- moved "ui" dir to "public" dir
- renamed "public" to "client"
- updated nodePong.js with new directory name
- cleaned gitlab repo


### **v0.0.1:**
- added package directories structure

  - "public": 
    - index.html (main html file)
    - "css" subdir
      - main.css (main css file for index.html)
    - "src" subdir:
      - jquery-2.1.3.min.js (not really needed)

  - "server":
    - server_utils.js (server utilities function)
    - server_params.js (Server parameters)

  - "ui": (empty, and will become a "public" subdir)

- added info files README.md, version.md, package.json and LICENSE 

- added nodejs application 'nodePong.js'