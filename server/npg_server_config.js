/**
 * @file npg_server_config.js
 * @author frtk
 */

var Config = {
  VERSION : '0.1.5',
  HTTP: {
    host: '127.0.0.1',
    port: 8042
  },
}



/**
 *
 * EXPORTS
 *
 */
if (typeof exports !== "undefined") {
  exports.Config = Config;
}
