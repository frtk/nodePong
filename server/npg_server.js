/**
 * @file npg_server.js
 * @author frtk                                                                                 
 */



/**
 *
 * nodePong Server Object
 *
 */
var NPGServer = {
  
  /*
   * Data 
   */
  // app
  version: '',
  host: '',
  port: 0,

  // Users and Games
  users: [],
  games: [],


  /*
   *
   */
  //--- init()
  init: function(o) {
    var self = this;
    self.version = o.VERSION || '';  
    self.host = o.HTTP.host || ''; 
    self.port = o.HTTP.port || 8042;  
    //
    self.users = []; 
    //
    self.log('$ ##### nodePong - v'+self.version);  
  },

  //--- setVersion(s)
  setVersion: function(s) {
    this.version = s;
  },


  /*
   * Server logs
   */ 
  //--- 
  log: function(msg) {
    var self = this;
    return console.log(self.newDateToString() + '$ ' + msg);
  },
  //--- dateToString
  dateToString: function(date) {
    var self = this;
    return '[' + self.addZero(date.getHours(), 'h') + ':' 
               + self.addZero(date.getMinutes(), 'm') + ':' 
               + self.addZero(date.getSeconds(), 's') + ':' 
               + self.addZero(date.getMilliseconds(), 'ms') + ']';
  },
  //--- addZero
  addZero: function(value, type) {
    switch(type) {
      case 'h':
      case 'm':
      case 's':
        if (value < 10) value = '0' + value;
          break;
      case 'ms':
        if (value < 10) value = '00' + value;
        else if (value >= 10 && value < 100) value = '0' + value;
        break;
      default:
        break;                  
    };
    return value;
  },
  //--- return new date string
  newDateToString: function() {
    var self = this;  
    return self.dateToString(new Date());
  },


   
  /*
   * Socket.io handling
   */ 
  //--- 
  socketHandling: function(io) {
    var self = this;
    io.sockets.on('connection', function (socket) {

      // 'disconnect'
      socket.on('disconnect', function () { 
        self.clientDisconnect(socket.id);
      });

      // 'registration'
      socket.on('regRequest', function (data) {
         self.userRegistration(data, socket);
      });


      //  
    });
  },


  /*
   * Socket handling utils
   */ 
  //
  userRegistration : function(data, socket) {
    var self = this;
    self.log('[Client:' + socket.id + '] New user registration attempt: name=' + data);
    if (!self.isNewUser(data)) {
      self.resetClientUI(socket);
      self.log('Server: User name already taken - reseting client UI');     
    } else {
      self.createUser(data, socket);
      self.updateAllClientsServerInfos();
      //
      self.sendMsgToSocket(socket, 'regDone');    
    }
  },

  //
  clientDisconnect : function(id) {
    var self = this;
    var u = self.getUserBySocketId(id);
    if (self.isUser(id)) { 
      self.log('[Client:' + id + '] User ' + u.name + ' disconnected');
      self.deleteUser(u.name);
      //
      self.updateAllClientsServerInfos();
    }
  },

   
  /*
   * User handling utils
   */
  //---
  sendMsgToSocket: function(socket, msg, data) {
    if (data !== undefined) {
      socket.emit(msg);
    } else {
      socket.emit(msg, data);
   }
  },

  //---
  isNewUser: function(name) {
    var self = this;
    var isNew = true;
    self.log('Server: Checking if player name is already used'); 
    for (u in self.users) {
      if (self.users[u].name == name) isNew = false;
    }
    return isNew;
  },
 
  //
  isUser: function(id) {
    var self = this;
    var isOn = false;
    for (u in self.users) {
      if (self.users[u].socket.id == id) isOn = true;
    }
    return isOn;
  },
 
  //---
  getUserByName: function(name) {
    var self = this;
    for (u in self.users) {
      if (self.users[u].name == name) return self.users[u];
    }
    return undefined;
  },

  //---
  getUserBySocketId: function(id) {
    var self = this;
    for (u in self.users) {
      if (self.users[u].socket.id == id) return self.users[u];
    }
    return undefined;
  },

  //
  resetClientUI: function(socket) {
    var self = this;
    self.sendMsgToSocket(socket, 'regNameTaken');
  },

  //
  createUser: function(name, socket) {
    var self = this;
    self.log('Server: New player registered, name=' + name + ' socketId= '+ socket.id);
    self.users.push(new User(name, socket));
    self.log('Server: number of players: n= ' + self.users.length);
    self.log('Server: player registered, send user ' + name + ' to menu page.');
  },
   
  //
  deleteUser: function(name) {
    var self = this;
    for (u in self.users) {
      if (self.users[u].name == name) {
        self.users[u].sendMsg('removedUser'); 
        self.users.splice(u,1);                                                              
        self.log('Server - Removed user  ' + name);
        self.log('Server - Number of players: n= ' + self.users.length);
      }
    }
  },



  /**
   *
   * Server Infos (players and games)
   *
   */
  //---
  updateAllClientsServerInfos: function(socket) {
    var self = this;
    for (u in self.users) {
      self.users[u].sendMsg('serverInfosUpdate', { infos: self.getServerInfos(), games: self.getGamesList() });
    }
    self.log('Server: updated server infos for all clients');
  },

  //---
  updateClientServerInfos: function(socket) {
    var self = this;
    socket.emit('serverInfosUpdate', { infos: self.getServerInfos(), games: self.getGamesList() });
    self.log('Server: update client server info');
  },

 //---
  getServerInfos: function() {
    var self = this;
    return {
      nUsers: self.nPlayers(),
      nGamesRunning: self.nGamesRunning(),
      nGamesAvail: self.nGamesAvail()
    } 
  },
 
  //---
  getGamesList: function() {
    var self = this;
    var res = [];
    for (g in self.games) {
      res.push(self.games[g].getHostName());
    } 
    return res;
  },
    
  //---
  nPlayers: function() {
    var self = this;
    return self.users.length;
  },
    
  //---
  nGamesRunning: function() {
    var self = this;
    var n = 0;
    for (g in self.games) {
      if (self.games[g].status == 'running') n++;
    }  
    return n;
  },

  //---
  nGamesAvail: function() {
    var self = this;
    var n = 0;
    for (g in self.games) {
      if (self.games[g].status == 'available') n++;
    }  
    return n;
  },


};




/**
 *
 * nodePong User Object
 *
 */
var User = function(name, socket) {
  //
  this.name = (name !== undefined) ? name : '';  
  this.socket = (socket !== undefined) ? socket : '';  
  //
  this.status = '';
};
//
User.prototype.constructor = User;
//
User.prototype.sendMsg = function(msg, data) {
  var self = this;
  self.socket.emit(msg, data);
};




/**
 *
 * nodePong Game Object
 *
 */
var Game = function() {
};




/**
 *
 * EXPORT
 *
 */
if (typeof exports !== "undefined") {
  exports.NPGServer = NPGServer;
  exports.User = User;
  exports.Game = Game;
}
