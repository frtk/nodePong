# **nodePong**

nodePong is a multiplayer online Pong game based on socket.io and nodejs.
Users can play or spectate games.


# Install

clone project from git

```
git clone https://git.tetalab.org/frtk/nodePong.git
```

The app has a few nodejs packages dependencies. If you want to install them manually, follow the instructions below, else use the "node_modules" directory from the cloned project.

```
cd nodePong
npm install fs
npm install express
npm install http
npm install socket.io
```


# Parameters

- Server side: Parameters can be tweaked from "nodePong/server/server_config.js"   
  - *port* : http and iostream services port (default: 8042)
  - ...


- Client side: Parameters can be tweaked from "nodePong/client/ui/client_config.js"
  - ...

  

# Run

From the nodePong directory,

```
nodejs nodePong.js
```