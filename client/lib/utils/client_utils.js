/**
 * @file client_utils.js
 * @author frtk@tetalab
 */

NPGClient.Utils = {

  /**
   * UI draw
   */
  //
  log: function(t) {
    console.log('[NPGClient]' + t);
  },


  /**
   * UI draw
   */
  //
  drawText: function(ctx, x1, y1, t, s, c) {
    ctx.font = s;
    ctx.fillStyle = c;      
    ctx.fillText(t, x1, y1);
  },

  //
  drawFillRect: function(ctx, x1, y1, w, h) {
    ctx.fillRect(x1, y1, w, h);
  },

  //
  drawRect: function(ctx, x1, y1, w, h) {
    ctx.beginPath();
    ctx.rect(x1, y1, w, h);
    ctx.stroke();
  },

  //
  drawLine: function(ctx, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
  },

  //
  drawSemiCircle: function(ctx, x, y, r, a1, a2, o, w, c) {
    ctx.lineWidth = w;
    ctx.strokeStyle = c;
    ctx.beginPath();
    ctx.arc(x, y, r, a1, a2, o);
    ctx.closePath();
    ctx.stroke();
  },

  drawFillCircle: function(ctx, x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, Math.PI*2, false);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
  },   

  //
  drawCircle: function(ctx, x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, Math.PI*2, false);
    ctx.closePath();
    ctx.stroke();
  },


  /**
   * UI Style
   */
  //  
  setTxtStyle : function(ctx, style) {
    ctx.font = style.font;
    ctx.fillStyle = style.col;       
    ctx.textAlign = style.align;               
  },      

  //  
  applyStyle : function(ctx, style) {
    //
    if (style.bm) {
      ctx.lineWidth = style.bw;
      ctx.strokeStyle = style.bc;
    }
    //
    if (style.fm) {
      ctx.fillStyle = style.fc;
    }
  },      


  /**
   * User Name 
   */
  //  
  addChar : function(key) {
    //console.log(key + ' ' + NPGClient.userName);
    if (NPGClient.userName.length < NPGClient.NAMEMAXSIZE) {
      NPGClient.userName = NPGClient.userName + String.fromCharCode(key).toLowerCase();   
      //console.log(NPGClient.userName);
    }
  },

  //
  removeChar: function(key) {
    if (NPGClient.userName.length > 0) {
      NPGClient.userName = NPGClient.userName.substring(0, NPGClient.userName.length - 1);      
      //console.log(NPGClient.userName);
    }                                                               
  },
  
  //
  validChar: function(key) {
    return NPGClient.evtHandler.loginValidKey(key);
  },

  //
  validName: function() {
    var len = NPGClient.userName.length;
    return (len >= 3 && len <= NPGClient.NAMEMAXSIZE);
  },

  //
  maxNameSize: function() {
    return NPGClient.userName.length == NPGClient.NAMEMAXSIZE;
  },              

  //
  resetName: function() {
    NPGClient.userName = '';
  },
  
  //
  nameEmpty: function() {
    return (NPGClient.userName.length == 0);
  },

}
