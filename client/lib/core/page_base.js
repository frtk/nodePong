/**
 * @file AppPage.js
 * @author frtk@tetalab
 */

NPGClient.AppPage = function(n) { 
    
  Object.defineProperty(this, 'id', { value: NPGClient.pageCount++ });
   
  this.name = n || '';
  this.idx = NPGClient.pageCount;   

  // ui elements   
  this.uiElems = [];

};

NPGClient.AppPage.prototype = {

  constructor: NPGClient.AppPage,

  //
  addUIObject: function(c) {
    var self = this;
    self.uiElems.push(c);
  },

  //
  getUIElems: function() {
    var self = this;
    return self.uiElems;     
  },

  //
  hasUIElem: function(name) {
    var self = this;
    if (self.uiElems.length > 0) {
      for (var i = 0; i < self.uiElems.length; i++)
 	if (self.uiElems[i].name == name) return true;
    }
    return false;
  },


  //
  getUIElemByName: function(name) {
    var self = this;
    if (self.uiElems.length > 0) {
      for (var i = 0; i < self.uiElems.length; i++)
 	if (self.uiElems[i].name == name) return self.uiElems[i];
    }
    return undefined;
  },

  // 
  printInfo: function() {
    var self = this;
    console.log('[NPGClient.AppPage] name:' + self.name + ', nUIElems=' + self.pages.length);

  }

};

