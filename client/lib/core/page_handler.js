/**
 * @file page_handler.js
 * @author frtk@tetalab
 */

NPGClient.PageHandler = {

  currPage: '',
  pages: [],  
    
  //
  setup: function() {
    var self = this; 
    self.currPage = NPGClient.LOGIN.NAME;
    // create login page 
    self.createLoginPage();
    // create start menu page
    self.createStartMenuPage();
  },

  //
  setCurrPage: function(n) {
    var self = this; 
    self.currPage = n;
  },

  //
  moveToPage: function(n) {
    var self = this; 
    switch (n) {
      case NPGClient.LOGIN.NAME:
        NPGClient.Utils.resetName();
        NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').reset();
        self.setCurrPage(NPGClient.LOGIN.NAME); 
        break;
      default: 
        break;
    }
  },

  //
  getCurrPageUIElems: function() {
    var self = this;
    return self.getPageByName(self.currPage).getUIElems();
  },

  //
  getCurrPageUIElemByName: function(name) {
    var self = this;
    if (self.pages.length > 0) {
      for (var i = 0; i < self.pages.length; i++) {
        if (self.pages[i].name == self.currPage) {
          return self.pages[i].getUIElemByName(name);
        }
      }
    }
    return undefined;
  },

  //
  getUIElem: function(name) {
    var self = this;
    var elem = [];
    if (self.pages.length > 0) {
      for (var i = 0; i < self.pages.length; i++) {
        elem = self.pages[i].getUIElems();
        for (e in elem) {
          if (elem[e].name == name) return elem[e]; 
        }
      }
    }
    return undefined;
  },

  // @need rework
  getUIElemFromPage: function(elem, page) {
    var self = this;
    if (self.pages.length > 0) {
      for (var i = 0; i < self.pages.length; i++) {
          if (page == self.pages[i].name) {
	      //	      return self.pages[i];
	  }
      }
    }
    return undefined;
  },


  //
  getPageByName: function(name) {
    var self = this;
    if (self.pages.length > 0) {
      for (var i = 0; i < self.pages.length; i++) {
        if (name == self.pages[i].name) return self.pages[i];
      }
    }
    return undefined;
  },

  // 
  updateServerInfos: function() {
    var self = this;   
    // server infos
    self.getUIElem('servinfo_users').update(NPGClient.SERVER.nPlayers);   
    self.getUIElem('servinfo_games_running').update(NPGClient.SERVER.nGamesRunning);   
    self.getUIElem('servinfo_games_avail').update(NPGClient.SERVER.nGamesAvail);   
    
  },



  // create login page
  createLoginPage: function() {
    //
    console.log('[NPGClient] Creating Login Page');
    var self = this;
    var p = new NPGClient.AppPage(NPGClient.LOGIN.NAME);
    // Title label     
    //p.addUIObject(new NPGClient.UILabel('login_title', NPGClient.LOGIN.TITLE));
    p.addUIObject(new NPGClient.UILabel(NPGClient.LOGIN.TITLE));    
    // name input
    p.addUIObject(new NPGClient.UIInputText('login_input', NPGClient.LOGIN.INPUT));
    // Server status
    p.addUIObject(new NPGClient.UIStatusText('login_servstat', NPGClient.LOGIN.SERVSTATUS));    
    // test cursor
    p.addUIObject(new NPGClient.UICursor(NPGClient.LOGIN.CURSOR)); 
    //
    self.pages.push(p);  
  },


  // create login page
  createStartMenuPage: function() {
    //
    console.log('[NPGClient] Creating Start Menu Page');
    var self = this;
    var p = new NPGClient.AppPage(NPGClient.STARTMENU.NAME);
    // Title label     
    //p.addUIObject(new NPGClient.UILabel('startmenu_title', NPGClient.STARTMENU.TITLE));
    p.addUIObject(new NPGClient.UILabel(NPGClient.STARTMENU.TITLE));
    // Start Menu
    p.addUIObject(new NPGClient.UIMenu(NPGClient.STARTMENU.MENU));
    // Server Players and Games info
    p.addUIObject(new NPGClient.UIStatusValue(NPGClient.STARTMENU.SERVINFO_USERS));
    p.addUIObject(new NPGClient.UIStatusValue(NPGClient.STARTMENU.SERVINFO_GAMES_RUNNING));
    p.addUIObject(new NPGClient.UIStatusValue(NPGClient.STARTMENU.SERVINFO_GAMES_AVAIL));
    //
    self.pages.push(p);  
  },




}; 

