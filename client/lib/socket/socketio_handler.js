/**
 * @file SocketIO.js
 * @author frtk
 */

NPGClient.SocketIO = { 

  // Connection to server  
  conn_nAttempts: 0,
  conn_IntervalID: 0,
  socket: {},
  isConnected: false,

  // start connection loop
  initConnectLoop:  function() { 
    var self = this;
      console.log();  
      self.startConnectLoop();
  },

  // start connection loop
  startConnectLoop:  function() { 
    var self = this;
    self.conn_IntervalID = setInterval(function() { self.connect(); }, 1500); 
  },

  // stop connection loop
  stopConnectLoop:  function() { 
    var self =  this;
    clearInterval(self.conn_IntervalID);
    self.conn_IntervalID = 0;
    self.conn_nAttempts = 0;
  },

  // connect socket
  connect: function() {
    var self = this;  
    // check for connection
    if (self.isConnected) {
      return false;  
    }
    //
    if (typeof io !== 'object') {
      //console.log('[NPGClient.SocketIO] io object is unknown');
    }
    if ((typeof self.socket.socket === 'undefined')) {      
      console.log('[NPGClient.SocketIO] attempting server connection...');
      self.socket = io.connect();  
      self.defineSocketMsgs();
    } else if (!self.socket.socket.connected && !self.socket.socket.connecting) {
      console.log('[NPGClient.SocketIO] attempting server connection...');
      self.socket.socket.connect();   
    }
  },

  // Define Socket Messages
  defineSocketMsgs: function() {
    var self = this;  

    if (typeof self.socket === 'undefined') {
      return false;
    }
    NPGClient.Utils.log('Deploying socket object features');

    // 'connecting' 
    self.socket.on('connecting', function () {
      NPGClient.Utils.log(' Connecting ...');
    });

    // 'connect'
    self.socket.on('connect', function() {
      NPGClient.Utils.log(' Connected to server');
      self.stopConnectLoop();
      self.isConnected = true;
    });

    // 'disconnect'
    self.socket.on('disconnect', function() {
      NPGClient.Utils.log(' > Server: socket disconnected');
      self.startConnectLoop();
      self.isConnected = false;
    });      

    // 'serverInfosUpdate'
    self.socket.on('serverInfosUpdate', function(data) {
      NPGClient.Utils.log(' > Server infos update. ' + data);
      NPGClient.SERVER.updateInfos(data);
      NPGClient.PageHandler.updateServerInfos();
    });     

    // 'regDone'
    self.socket.on('regDone', function() {
      NPGClient.Utils.log(' > Server: User registration done');
      NPGClient.PageHandler.setCurrPage(NPGClient.STARTMENU.NAME);
    });     

    // 'regNameTaken'
    self.socket.on('regNameTaken', function() {
      NPGClient.Utils.log(' > Server: login name already used.');
      NPGClient.PageHandler.moveToPage(NPGClient.LOGIN.NAME);
    });     

  },

  // send message to server
  sendMsg: function(name, data) {
    var self = this;  
    self.socket.emit(name, data);
  }
  
};
