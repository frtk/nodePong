/**
 * @file event_handling.js
 * @autour frtk@tetalab
 */

NPGClient.evtHandler = {

  keyState: {},
  
  //
  init: function() {
    var self = this;
    //
    self.keyState = {};
    for (k in NPGClient.KEYS) {
      self.keyState[NPGClient[k]] = false;
    }               
    //
    document.addEventListener('keydown',function(e) {
      self.keyState[e.keyCode || e.which] = true;
      self.onKeyDown(e);
    }, true);
    //
    document.addEventListener('keyup',function(e){
      self.keyState[e.keyCode || e.which] = false;
    }, true);                       
  },

  // 
  isValidKey: function(k) {
    return (k >= 48 && k <= 90);
  },
   
  //
  onKeyDown: function(evt) {
    var self = this;
    if (evt.keyCode == NPGClient.KEYS.ESC) {
      //self.playerLogout();   // player logout
    } else if (evt.keyCode == NPGClient.KEYS.F1) {
      //self.sendToMenuPage(); // back to previous page
    } else {
      switch (NPGClient.PageHandler.currPage) {
        case NPGClient.LOGIN.NAME:
          self.userLogin(evt);            
          break;
        case NPGClient.STARTMENU.NAME:
          self.startMenu(evt);            
          break;
        default:
          break;
      }
    }
  },


  /*
   * 
   */
  //
  userLogin : function (evt) {
    
    var self = this;
    //
    switch (evt.keyCode) {
      case NPGClient.KEYS.ENTER:
        if (NPGClient.SocketIO.isConnected) {                         
          if (NPGClient.userName.length != 0) {
            NPGClient.SocketIO.sendMsg('regRequest', NPGClient.userName);
          }
        } else {
          NPGClient.Utils.resetName();  
          NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').reset();
        }                               
        break;
      case NPGClient.KEYS.BACKSPACE:
        //--- remove a character        
        evt.preventDefault();
        if (!NPGClient.Utils.nameEmpty()) {
          NPGClient.Utils.removeChar();
          NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').translateX(-15);
          NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').resetBlink();
        }
        break;
      default:
        //--- add character
        if (self.isValidKey(evt.keyCode) && !NPGClient.Utils.maxNameSize()) {
          NPGClient.Utils.addChar(evt.keyCode);
          newNameW = NPGClient.ui.ctx.measureText(NPGClient.userName).width;
          NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').translateX(15);
          NPGClient.PageHandler.getCurrPageUIElemByName('login_cursor').resetBlink();  
        }
        break;
    }
  },


  /*
   * 
   */
  //
  startMenu : function(evt) {
    var self = this;
    switch (evt.keyCode) {		
      case NPGClient.KEYS.ARROW_DOWN :
        NPGClient.PageHandler.getCurrPageUIElemByName(NPGClient.STARTMENU.MENU.name).selectNextItem();
        break;
      case NPGClient.KEYS.ARROW_UP :
        NPGClient.PageHandler.getCurrPageUIElemByName(NPGClient.STARTMENU.MENU.name).selectPrevItem();
        break;
      case NPGClient.KEYS.ENTER :
        //
/* 
        switch (self.cursor.menuItemRef.NAME) {
          case 'CREATE':
            self.socket.emit('createGame');
            break;
          case 'JOIN':
            self.socket.emit('joinGameMenu');
            break;
          case 'WATCH':
            self.socket.emit('watchGameMenu');
            break;
          default:
            break;
        }
        break;
*/
      default:
        break;
    }	
  },


}
