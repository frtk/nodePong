/**
 * @file ui.js
 * @author frtk@tetalab
 */

NPGClient.ui = { 

  // canvas 
  canvas_id: '',
  canvas_bcol: '',
  canvas: {},
  ctx: {},

  // draw loop paramaters
  loopIntervalId: 0,
  loopDelay: 40, 


  /* 
   * init
   */
  //
  setupUI: function(id, w, h, c) {
    var self = this;
    self.canvas_id = id;
    self.canvas = document.getElementById(id); 
    self.ctx = self.canvas.getContext("2d");
    self.canvas.width = w;
    self.canvas.height = h;
    self.canvas_bcol = c;
    // start draw loop
    self.startDrawLoop();
  },


  /* 
   * Draw loop
   */
  //
  setLoopDelay: function(d) {
    this.loopDelay = d;
  },

  //
  startDrawLoop: function() {
    var self = this;
    self.loopIntervalId = setInterval(function() { 
      self.run(); 
    }, self.loopDelay);
  },

  //
  stopDrawLoop: function() {
    var self = this;
    clearInterval(self.loopIntervalId); 
    self.loopIntervalId = 0;            
  },

  //
  run: function() {
    var self = this;
    self.drawPage();
  },


  /* 
   * Draw functions
   */
  //
  drawPage: function() {
    var self = this;
    self.clear();
    self.drawUIBackground();
    self.drawUIObjects();	
  },

  //
  drawUIBackground: function() {
    var self = this;
    self.ctx.fillStyle = self.canvas_bcol;
    self.ctx.fillRect(0, 0, self.canvas.width, self.canvas.height);
  },

  //
  drawUIObjects: function() {
    var self = this;
    var elems = NPGClient.PageHandler.getCurrPageUIElems();  
    if (elems !== undefined && elems.length > 0) {
	for (var i = 0; i < elems.length; i++) elems[i].draw(self.ctx);
    }
  },

  //
  clear: function() {
    var self = this;
    self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height); 
  },

    
};

