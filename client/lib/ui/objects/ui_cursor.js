/**
 * @file ui_cursor.js
 * @author frtk@tetalab
 */

NPGClient.UICursor = function(o) { 

  // inheritance
  NPGClient.UIObject.call(this, o);

  // obj name
  this.name = o.name !== undefined ? o.name : '';
  this.w = o.w !== undefined ? o.w : 0;
  this.h = o.h !== undefined ? o.h : 0;
  // starting position
  this.x0 = this.x();
  this.y0 = this.y();
  // style
  this.style = o.style !== undefined ? o.style : {};
  // state
  this.bState = true;
  this.bCount = 0;
  this.bCountMax = 5;

};

// inheritance
NPGClient.UICursor.prototype = Object.create(NPGClient.UIObject.prototype);

// constructor
NPGClient.UICursor.prototype.constructor = NPGClient.UICursor;

// reset
NPGClient.UICursor.prototype.reset = function() {
  var self = this;
  self.moveTo(self.x0, self.y0);
  self.bState = true;
  self.bCount = 0;
}

// resetBlink
NPGClient.UICursor.prototype.resetBlink = function() {
    var self = this;
    self.bState = true;
    self.bCount = 0;
}


// draw
NPGClient.UICursor.prototype.draw = function(ctx) {
  var self = this;
  if (self.bState) {
    if (self.bCount == self.bCountMax) {
      self.bCount = 0;
      self.bState = false;
    } else {
      NPGClient.Utils.drawFillRect(ctx, self.x(), self.y(), self.w, self.h);
      self.bCount++;
    }
  } else {
    if (self.bCount == self.bCountMax) {
      self.bCount = 0;
      self.bState = true;
      NPGClient.Utils.drawFillRect(ctx, self.x(), self.y(), self.w, self.h);
    } else self.bCount++;
  }
};




