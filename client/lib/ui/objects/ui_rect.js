/**
 * @file ui_rect.js
 * @author frtk@tetalab
 */

NPGClient.UIRect = function(n, o) { 

  //
  NPGClient.UIObject.call(this, o);
  // obj name
  this.name = n !== undefined ? n : '';
  this.w = o.w !== undefined ? o.w : 0;
  this.h = o.h !== undefined ? o.h : 0;
  // style
  this.style = o.style !== undefined ? o.style : new NPGClient.UIStyle(); 
};

// inheritance
NPGClient.UIRect.prototype = Object.create(NPGClient.UIObject.prototype);

// Constructor
NPGClient.UIRect.prototype.constructor = NPGClient.UIRect;
   
NPGClient.UIRect.prototype.draw = function(ctx) {
  var self = this;
  NPGClient.Utils.applyStyle(ctx, self.style);
  if (!self.style.fm) {
    NPGClient.Utils.drawRect(ctx, self.x(), self.y(), self.w, self.h);
  } else {
    NPGClient.Utils.drawFillRect(ctx, self.x(), self.y(), self.w, self.h);
  }
};
