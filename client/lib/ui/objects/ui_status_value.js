/**
 * @file ui_status_value.js
 * @author frtk@tetalab
 */

NPGClient.UIStatusValue = function(o) { 
  // 
  NPGClient.UIObject.call(this, o);
  // obj name
  this.name = o.name !== undefined ? o.name : '';
  // pos
  this.x = o.x !== undefined ? o.x : 0; 
  this.y = o.y !== undefined ? o.y : 0; 
  //
  this.s = o.style !== undefined ? o.style : '';
  // label
  this.label = o.label !== undefined ? o.label : '';
  // widget var value
  this.value = 0;

};


/**
 * Inheritance
 */
NPGClient.UIStatusValue.prototype = Object.create(NPGClient.UIObject.prototype);


/**
 * Functions 
 */
//
NPGClient.UIStatusValue.prototype.constructor = NPGClient.UIStatusValue;
 
//
NPGClient.UIStatusValue.prototype.update = function(v) {
  var self = this;
  self.value = v;
};

//
NPGClient.UIStatusValue.prototype.draw = function(ctx) {
  var self = this;
  NPGClient.Utils.setTxtStyle(ctx, self.s); 
  //
  var txt = self.label + ' ' + self.value;
  //
  ctx.fillText(txt, self.x, self.y);

};

