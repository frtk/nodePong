/**
 * @file ui_menu.js
 * @author frtk@tetalab
 */

NPGClient.UIMenu = function(o) { 
  // 
  NPGClient.UIObject.call(this, o);
  // obj name
  this.name = o.name !== undefined ? o.name : '';
  // type (vertical/horizontal)
  this.type = o.type !== undefined ? o.type : ''; 
  // pos
  this.x = o.x !== undefined ? o.x : 0; 
  this.y = o.y !== undefined ? o.y : 0; 
  // 
  this.itemPadding = o.padding !== undefined ? o.padding : 0;
  //
  this.s = o.style !== undefined ? o.style : '';
  // item list 
  //console.log(o.items + ' ' + o.items.length);
  this.items = o.items !== undefined ? o.items : [];
  //console.log(this.items + ' ' + this.items.length);
  // selected item
  this.currItemIdx = 0; 
  //
  this.hasCursor = o.cursor !== undefined ? true : false;
  this.cursor = o.cursor !== undefined ? new NPGClient.UICursor(o.cursor) : {};

};

//
NPGClient.UIMenu.prototype = Object.create(NPGClient.UIObject.prototype);

//
NPGClient.UIMenu.prototype.constructor = NPGClient.UIMenu;
 
// 
NPGClient.UIMenu.prototype.addItem = function(t) {
  var self = this;
  self.items.push(t);   
};

// 
NPGClient.UIMenu.prototype.selectPrevItem = function(t) {
  var self = this;
  var n = self.items.length;
  var cShift = 0;
  if (n != 0) {
    if (self.currItemIdx == 0) {
      self.currItemIdx = n - 1; 
      cShift = self.currItemIdx*self.itemPadding; 
    } else {
      self.currItemIdx--;
      cShift = -1*self.itemPadding;    
    }
    if (self.hasCursor) {
      self.cursor.translateY(cShift);
      self.cursor.resetBlink();
    }
  }   
};


// 
NPGClient.UIMenu.prototype.selectNextItem = function(t) {
  var self = this;
  var n = self.items.length;
  var cShift = 0;
  if (n != 0) {
    if (self.currItemIdx == n - 1) {
      self.currItemIdx = 0;
      cShift = -1*(n - 1)*self.itemPadding; 
    } else {
      self.currItemIdx++;
      cShift = self.itemPadding;    
    }
    if (self.hasCursor) {
      self.cursor.translateY(cShift);
      self.cursor.resetBlink();
    }
  }   
};


//
NPGClient.UIMenu.prototype.draw = function(ctx) {
  var self = this;
  var y = self.y;  
  var ycurs = 0;
  NPGClient.Utils.setTxtStyle(ctx, self.s); 
  // menu
  if (self.items !== undefined && self.items.length > 0) {
    for (var i = 0; i < self.items.length; i++) {
      ctx.fillText(self.items[i], self.x, y + i*self.itemPadding);
    }      
  }
  // cursor if needed
  //var yc = 0;
  if (self.hasCursor) {
    //yc = self.cursor.y + self.cursorMov*self.itemPadding;  
    //self.cursor.translateY(yc);
    self.cursor.draw(ctx);  
  }

};

