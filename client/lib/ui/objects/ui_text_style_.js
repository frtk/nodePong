/**
 * @file ui_text_style.js
 * @author frtk@tetalab
 */

NPGClient.UITextStyle = function(f, c, a) { 

  this.font = f !== undefined ? f : '';
  this.color = c !== undefined ? c : '';
  this.align = a !== undefined ? a : '';

};

NPGClient.UITextStyle.prototype = {
    
  constructor: NPGClient.UITextStyle,
    
};

