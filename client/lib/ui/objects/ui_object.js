/**
 * @file ui_object.js
 * @author frtk@tetalab
 */
//

NPGClient.UIObject = function(o) { 

  this._x = (o.x !== undefined) ? o.x : 0;  // x position
  this._y = (o.y !== undefined) ? o.y : 0;  // y position
  this._theta = (o.theta !== undefined) ? o.theta : 0;  // theta wrt horizon

};


/**
 * 
 */
NPGClient.UIObject.prototype = {

  // Constructor
  constructor: NPGClient.UIObject,

  //set/get  
  x: function(data) {
    var self = this;

    if (data !== undefined) {
      self._x = data;
    } else {
      return self._x;	
    }

  },

  //set/get  
  y: function(data) {
    var self = this;

    if (data !== undefined) {
      self._y = data;
    } else {
      return self._y;	
    }

  },

  //
  moveTo: function(x, y) {
    var self = this;
    self._x = x;
    self._y = y;
  },

  //
  translateXY: function(x, y) {
    var self = this;
    self._x += x;
    self._y += y;
  },

  //
  translateX: function(x) {    
    this._x += x;
  }, 
   
  //
  translateY: function(y) {
    this._y += y;
  },

  //
  rotate: function(x, y, t) {
  },


}; 
