/**
 * @file ui_label.js
 * @author frtk@tetalab
 */

NPGClient.UILabel = function(o) { 
  // 
  NPGClient.UIObject.call(this, o);
  // obj name
  this.name = o.name !== undefined ? o.name : '';
  // pos
  this.x = o.x !== undefined ? o.x : 0; 
  this.y = o.y !== undefined ? o.y : 0; 
  // text
  this.text = o.text !== undefined ? o.text : '';
  // text style
  this.s = o.style !== undefined ? o.style : '';

};


/**
 * Inheritance
 */
NPGClient.UILabel.prototype = Object.create(NPGClient.UIObject.prototype);


/**
 * Functions 
 */
//
NPGClient.UILabel.prototype.constructor = NPGClient.UILabel;

//
NPGClient.UILabel.prototype.draw = function(ctx) {
  var self = this;
  NPGClient.Utils.setTxtStyle(ctx, self.s);
  ctx.fillText(self.text, self.x, self.y);    

};

