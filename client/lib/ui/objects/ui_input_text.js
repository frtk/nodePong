/**
 * @file ui_input_text.js
 * @author frtk@tetalab
 */

NPGClient.UIInputText = function(n, o) { 

  // obj name
  this.name = n !== undefined ? n : '';
  // pos
  this.x = o.x !== undefined ? o.x : 0; 
  this.y = o.y !== undefined ? o.y : 0; 
    
  // text
  this.text = o.text !== undefined ? o.text : '';
  //
  this.input = '';
  
  // text style
  this.s = o.style !== undefined ? o.style : new NPGClient.UITextStyle();

};


NPGClient.UIInputText.prototype = {

  // Constructor
  constructor: NPGClient.UIInputText,
  
  //
  updateInput: function() {
    var self = this;
    self.input = NPGClient.userName;     
  },

  //
  draw: function(ctx, state) {
    var self = this;
    var t = self.text + self.input;
    self.updateInput();  
    //console.log(self.s);
    NPGClient.Utils.setTxtStyle(ctx, self.s);
    ctx.fillText(t, self.x, self.y);    
  }

};

