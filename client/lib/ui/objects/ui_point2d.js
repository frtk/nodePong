/**
 * @file ui_point2d.js
 * @author frtk@tetalab
 */

NPGClient.UIPoint2D = function(x, y) { 
  
  // (x,y) position
  this.x = x !== undefined ? x : 0;
  this.y = y !== undefined ? y : 0;

};

NPGClient.UIPoint2D.prototype = {
    
  // Constructor
  constructor: NPGClient.UIPoint2D,

  //
  set: function(x, y) {
    this.x = x;
    this.y = y;
  },

  //
  setX: function(x) {
    this.x = x;
  },
  
  //
  setY: function(y) {
    this.y = y;
  }
  

};

