/**
 * @file ui_status_text.js
 * @author frtk@tetalab
 */

NPGClient.UIStatusText = function(n, o) { 

  // obj name
  this.name = n !== undefined ? n : '';
  // pos
  this.x = o.x !== undefined ? o.x : 0; 
  this.y = o.y !== undefined ? o.y : 0; 
    
  // text
  this.text = o.text !== undefined ? o.text : '';
  //
  this.currText = 0;
  
  // text style
  this.s = o.style !== undefined ? o.style : new NPGClient.UITextStyle();

};


NPGClient.UIStatusText.prototype = {

  // Constructor
  constructor: NPGClient.UIStatusText,
  
  //
  updateState: function() {
    var self = this;
    if (NPGClient.SocketIO.isConnected) self.currText = self.text.online;
    else self.currText = self.text.offline;
  },

  //
  draw: function(ctx, state) {
    var self = this;
    self.updateState();  
    NPGClient.Utils.setTxtStyle(ctx, self.s);
    ctx.fillText(self.currText, self.x, self.y);    
  }

};

