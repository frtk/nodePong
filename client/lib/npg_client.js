/**
 * @file NPGClient.js
 * @author frtk
 */

var NPGClient = { 'version': '' };


// LOGIN NAME
NPGClient.NAMEMAXSIZE = 8;
NPGClient.userName = '';


// SERVER INFOS
NPGClient.SERVER = {
  isUp: false,
  nPlayers : 0,
  nGamesRunning : 0,
  nGamesAvail : 0,
  gamesList : [],
  updateInfos: function(data) {
    var self = this;
    self.nPlayers = data.infos.nUsers;
    self.nGamesRunning = data.infos.nGamesRunning;
    self.nGamesAvail = data.infos.nGamesAvail;
    self.gamesList = [];
    for (g in data.games) self.gamesList(data.games[g]);
  }, 
};


// UI
NPGClient.CAN_W = 800;
NPGClient.CAN_H = 500;
NPGClient.CAN_COL = '#000000';


/**
 * Keyboard events
 */ 
NPGClient.KEYS = {
  F1 : 112,
  SPACE : 32,
  ESC : 27,
  ARROW_UP : 38,
  ARROW_DOWN : 40,
  ENTER : 13,
  DELETE : 46,
  BACKSPACE : 8,
};


/**
 * APP PAGES
 */ 
NPGClient.pageCount = -1;
// Login Page
NPGClient.LOGIN = {
  NAME: 'login',
  SERVSTATUS: { 
    'text': {
      'online' : 'server: online',
      'offline' : 'server: offline',
    },
    'x': 400,
    'y': 450,
    'style': { 
      'font': '15px Arial',
      'col': '#FFFFFF',
      'align': 'center'
    },
  },
  TITLE: {
    'name': 'login_title',
    'text': 'nodePong',
    'x': 400,
    'y': 150,
    'style': { 
      'font': '50px Arial',
      'col': '#FFFFFF',
      'align': 'center'
    }
  },
  INPUT: {
    'text': 'login: ',
    'x': 285,
    'y': 300,
    'style': { 
      'font': '25px Monospace',
      'col': '#FFFFFF',
      'align': 'left'
    }
  }, 
  CURSOR: {
    'name': 'login_cursor',
    'w': 19,
    'h': 19,
    'x': 390,
    'y': 283,
    'style': { 
      'fm': true,
      'fc': '#FFFFFF',
      'bm': false,
      'bw': 0,
      'bc': '',
    }
  },  
};


/*
 * Start Menu Page
 */
NPGClient.STARTMENU = {
  NAME: 'start_menu',
  TITLE: {
    'name': 'start_menu_title',
    'text': 'nodePong',
    'x': 400,
    'y': 150,
    'style': { 
      'font': '50px Arial',
      'col': '#FFFFFF',
      'align': 'center'
    }
  },
  MENU: {
    'name': 'start_menu',
    'x': 350,
    'y': 250,
    'items': [
	'create game',
	'join game',
        'spectate game'
    ],
    'padding': 40,
    'style': { 
      'font': '25px Monospace',
      'col': '#FFFFFF',
      'align': 'left'
    },
    'cursor': {
      'name': 'start_menu_cursor',
      'w': 19,
      'h': 19,
      'x': 320,
      'y': 233,
      'style': { 
        'fm': true,
        'fc': '#FFFFFF',
        'bm': false,
        'bw': 0,
        'bc': '',
      }
    }
  },
  SERVINFO_USERS: { 
   'name': 'servinfo_users',
   'label': 'Users online:',
    'x': 70,
    'y': 470,
    'style': { 
      'font': '15px Monospace',
      'col': '#FFFFFF',
      'align': 'left'
    },
  },
  SERVINFO_GAMES_RUNNING: {
   'name': 'servinfo_games_running', 
   'label': 'Games running:',
    'x': 400,
    'y': 470,
    'style': { 
      'font': '15px Monospace',
      'col': '#FFFFFF',
      'align': 'center'
    },
  },
  SERVINFO_GAMES_AVAIL: { 
   'name': 'servinfo_games_avail',
   'label': 'Games available:',
    'x': 730,
    'y': 470,
    'style': { 
      'font': '15px Monospace',
      'col': '#FFFFFF',
      'align': 'right'
    },
  },

};



